import * as React from 'react';
import Wrapper from 'Shared/Components/Core/Wrapper';
import { styled } from 'styletron-react';
interface State {
    character: Api.Character;
}

const Form = styled('form', () => ({
    display: 'flex',
    justifyContent: 'space-around'
}));
const Column = styled('Div', () => ({
    display: 'flex',
    flexDirection: 'column',
    width: '42%',
    minWidth: '42%',
    maxWidth: '42%'
}));

class Login extends React.Component<any, State> {
    constructor() {
        super();

        this.state = {
            character: {}
        };
    }

    render() {
        return (
            <Wrapper
                type="FLEX_CONTAINER"
                options={['FLEX_COLUMN', 'ABSOLUTE_CENTER', 'SIZE_NORMAL']}>
                <h3>
                    Karaktär
                </h3>

                <Form>
                    <Column>
                        <Wrapper type="FLEX_WRAPPER">
                            <label>
                                Förnamn
                            </label>
                            <input />
                        </Wrapper>
                    </Column>
                    <Column>
                        <Wrapper type="FLEX_WRAPPER">
                            <label>
                                Efternamn
                            </label>
                            <input />
                        </Wrapper>
                    </Column>
                </Form >
            </Wrapper >
        );
    }
}

export default Login;
