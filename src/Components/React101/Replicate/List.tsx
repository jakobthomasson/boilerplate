import * as React from 'react';
import { Table, Thead, Tbody, Th, Tr } from 'Components/React101/styletron';
import { Technique } from 'Components/React101/types';
import ListItem from './ListItem';

import Test from 'Shared/Components/Core/Wrapper/test';
interface Props {
    techniques: Technique[];
    changeTechnique: (technique: Technique) => void;

}

class Example extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <Table>
                <Thead>
                    <Tr>
                        <Th>
                            Namn
                        </Th>
                        <Th>
                            Beskrivning
                        </Th>
                        <Th>
                            Typ av teknik
                        </Th>
                        <Th>
                            Betyg
                        </Th>
                    </Tr>
                </Thead>

                <Tbody>
                    {this.props.techniques.map((technique: Technique) => {
                        return (
                            <ListItem technique={technique} changeTechnique={this.props.changeTechnique} />
                        );

                    })

                    }
                </Tbody>
            </Table>
        );
    }
}

export default Example;
