import { Technique } from './types';

export const exampleTechniques: Technique[] = [
    {
        id: 1,
        name: 'React',
        description: 'FE technique for developing very nice applications.',
        rating: 'ej betygsatt',
        typeOf: 'Frontend'
    },
    {
        id: 2,
        name: 'Angular',
        description: 'FE technique which makes you hate yourself.',
        rating: 'ej betygsatt',
        typeOf: 'Frontend'
    },
    {
        id: 3,
        name: 'Dapper',
        description: 'BE technique to help you do stuff with databes.',
        rating: 'ej betygsatt',
        typeOf: 'Backend'
    },
    {
        id: 4,
        name: 'Dancing',
        description: 'You can dance if you want to, you can leave your friends behind. But if your friends dont dance, and if they dont dance well they are no friends of mine.',
        rating: 'ej betygsatt',
        typeOf: 'Other'
    },
    {
        id: 5,
        name: 'Befriending',
        description: 'How to make friends.',
        rating: 'ej betygsatt',
        typeOf: 'Other'
    },
    {
        id: 6,
        name: 'Listening',
        description: 'Not everyone can do it.',
        rating: 'ej betygsatt',
        typeOf: 'Other'
    },
];
