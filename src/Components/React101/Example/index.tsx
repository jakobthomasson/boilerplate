import * as React from 'react';
import { MainWrapper } from 'Components/React101/styletron';
import { Technique } from 'Components/React101/types';
import { exampleTechniques } from 'Components/React101//exampleData';
import List from './List';

interface State {
    techniques: Technique[];
}

class Example extends React.Component<any, State> {
    constructor() {
        super();

        this.state = {
            techniques: []
        };
    }

    componentDidMount() {
        this.setState({
            techniques: exampleTechniques
        });
    }

    changeTechnique = (technique: Technique) => {
        let techniques: Technique[] = this.state.techniques;
        let id: number;

        techniques.map((currentTech: Technique, index: number) => {
            if (technique.id === currentTech.id) {
                id = index;
            }
        });

        techniques[id] = technique;

        this.setState({ techniques });
    }

    render() {
        return (
            <MainWrapper>
                <List techniques={this.state.techniques} changeTechnique={this.changeTechnique} />
            </MainWrapper>
        );
    }
}

export default Example;
