import * as React from 'react';
import { Table, Thead, Tbody, Th, Tr } from 'Components/React101/styletron';
import { Technique } from 'Components/React101/types';
import ListItem from './ListItem';

interface Props {
    techniques: Technique[];
    changeTechnique: (technique: Technique) => void;
}

interface State {
    isOpen: boolean;
}

class Example extends React.Component<Props, State> {
    constructor() {
        super();

        this.state = {
            isOpen: false
        };
    }

    render() {
        console.log(this.props.techniques);
        return (

            <Table>
                <Thead>
                    <Tr>
                        <Th>
                            Namn
                        </Th>
                        <Th>
                            Beskrivning
                        </Th>
                        <Th>
                            Typ av teknik
                        </Th>
                        <Th>
                            Betyg
                        </Th>
                    </Tr>
                </Thead>

                <Tbody>
                    {
                        this.props.techniques.map((technique: Technique) => {
                            return (
                                <ListItem technique={technique} changeTechnique={this.props.changeTechnique} />

                            );
                        })}
                </Tbody>
            </Table>
        );
    }
}

export default Example;
