import * as React from 'react';
import { Tr, Td } from 'Components/React101/styletron';
import { Technique } from 'Components/React101/types';
import DropDown from 'Components/React101/DrowDown';
import { ratings } from 'Components/React101/types';

interface Props {
    technique: Technique;
    changeTechnique: (technique: Technique) => void;
}

class ListItem extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    changeRating = (rating: ratings) => {
        let technique: Technique = this.props.technique;
        technique.rating = rating;
        this.props.changeTechnique(technique);
    }

    render() {
        console.log(this.props.technique);
        return (
            <Tr>
                <Td cssProps={{ fontWeight: 'bold' }}>
                    {this.props.technique.name}
                </Td>
                <Td >
                    {this.props.technique.description}
                </Td>
                <Td>
                    {this.props.technique.typeOf}
                </Td>
                <Td>
                    <DropDown selectedRating={this.props.technique.rating} changeRating={this.changeRating} />
                </Td>
            </Tr>

        );
    }
}

export default ListItem;
