import * as React from 'react';
import { ratings } from 'Components/React101/types';
import { DropDownWrapper, DropDownOptionsWrapper, Text } from 'Components/React101/styletron';

interface Option {
    rating: ratings;
}
interface Props {
    selectedRating: ratings;
    changeRating: (rating: ratings) => void;
}

interface State {
    isOpen: boolean;
    dropDownOptions: Option[];
}

class DropDown extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let dropDownOptions: Option[] = [];
        dropDownOptions.push({ rating: 'ej betygsatt' });
        dropDownOptions.push({ rating: 'dålig' });
        dropDownOptions.push({ rating: 'medel' });
        dropDownOptions.push({ rating: 'bra' });

        this.state = {
            isOpen: false,
            dropDownOptions: dropDownOptions
        };
    }

    onClick() {
        this.setState({ isOpen: !this.state.isOpen });
    }

    onSelectRating(rating: ratings) {
        this.props.changeRating(rating);
    }

    render() {

        return (

            <DropDownWrapper onClick={() => this.onClick()}>
                <Text isSelected>
                    {this.props.selectedRating}
                </Text>

                {this.state.isOpen &&
                    <DropDownOptionsWrapper>
                        {this.state.dropDownOptions.map((option: Option) => {
                            return (
                                <Text onClick={() => this.onSelectRating(option.rating)}>
                                    {option.rating}
                                </Text>
                            );
                        })}
                    </DropDownOptionsWrapper>
                }
            </DropDownWrapper>
        );
    }
}

export default DropDown;
