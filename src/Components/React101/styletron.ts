import { styled } from 'styletron-react';
import { MEDIUM } from 'Shared/Styles/Settings/spacing';
import { BACKGROUND, SECONDARY_LIGHT, PRIMARY_LIGHT } from 'Shared/Styles/Settings/colors';
import { } from 'Shared/Styles/Settings/base';
import { BORDER_CONTAINER, BORDER_TABLE, BORDER_DOWN, BORDER_DROPDOWN } from 'Shared/Styles/Props/border';
import { MARGIN_MENU_HEIGHT_AUTO } from 'Shared/Styles/Props/margin';
import { PADDING_HUGE } from 'Shared/Styles/Props/padding';

export const MainWrapper = styled('div', () => ({
    width: '1000px',
    display: 'flex',
    justifyContent: 'center',
    background: BACKGROUND,
    ...PADDING_HUGE,
    ...MARGIN_MENU_HEIGHT_AUTO,
    ...BORDER_CONTAINER,
}));

export const Table = styled('table', () => ({
    tableLayout: 'fixed',
    width: '90%',
    ...BORDER_TABLE
}));

export const Thead = styled('thead', () => ({

}));

export const Tbody = styled('tbody', () => ({

}));

export const Tr = styled('tr', () => ({
    padding: MEDIUM,
    ...BORDER_DOWN
}));

export const Th = styled('th', () => ({
    textAlign: 'left',
    padding: MEDIUM,
    textTransform: 'uppercase'

}));

export const Td = styled('td', ({ cssProps }: { cssProps?: React.CSSProperties }) => ({
    verticalAlign: 'top',
    padding: MEDIUM,
    ...cssProps
}));

export const DropDownWrapper = styled('div', () => ({
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    cursor: 'pointer',
    padding: MEDIUM,
    ...BORDER_DROPDOWN
}));

export const DropDownOptionsWrapper = styled('div', () => ({

}));

export const Text = styled('p', ({ isSelected }: { isSelected?: boolean }) => ({
    marginBottom: 0,
    width: '100%',
    fontWeight: isSelected && 'bold',
    borderBottom: isSelected && `2px solid ${PRIMARY_LIGHT}`,
    ':hover': {
        background: !isSelected && SECONDARY_LIGHT
    }
}));
