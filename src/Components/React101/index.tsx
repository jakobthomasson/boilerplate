import * as React from 'react';
import Wrapper from 'Shared/Components/Core/Wrapper';
interface State {
    character: Api.Character;
}

class React101 extends React.Component<any, State> {
    constructor() {
        super();

        this.state = {
            character: {}
        };
    }

    render() {
        return (
            <Wrapper
                type="FLEX_CONTAINER"
                options={['FLEX_COLUMN', 'SIZE_NORMAL', 'ABSOLUTE_CENTER']}
            >
                <h3>
                    React101
                </h3>

            </Wrapper >
        );
    }
}

export default React101;
