type category = 'Frontend' | 'Backend' | 'Other';
export type ratings = 'ej betygsatt' | 'dålig' | 'medel' | 'bra';

export type Technique = {
    id: number;
    name: string;
    description: string;
    rating: ratings;
    typeOf: category;
};
