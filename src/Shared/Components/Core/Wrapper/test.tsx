import * as React from 'react';
import withProps, { StandardProps, Defaults } from 'Shared/Components/Core/HOC/withProps';
import { styled } from 'styletron-react';

type elementTypes = 'div' | 'ul' | 'li';

type Props = StandardProps & {
    elementType: elementTypes;
    name: string;
};

class Wrapper extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    CreateWrapper = (element: elementTypes) => {
        const Wra = styled(element, Defaults);
        return <Wra {...this.props} />;
    }

    render() {
        return this.CreateWrapper(this.props.elementType);

    }
}

export default withProps(Wrapper);
