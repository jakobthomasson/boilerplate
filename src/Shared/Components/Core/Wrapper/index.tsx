﻿import * as React from 'react';
import { styled } from 'styletron-react';
import * as types from './types';
import * as optionTypes from './options';
import OptionHandler from '../OptionHandler';


type Props = React.DOMAttributes<{}> & {
    type: types.WrapperTypes;
    options?: optionTypes.OptionTypes[];
};

const Wrapper = styled('div', ({
    type = 'DEFAULT',
    options = ['NONE']
}: Props) => ({
        ...types[type],
        ...new OptionHandler(options, 'Wrapper').cssProps
    }));

export default Wrapper;
