import * as React from 'react';
import * as margins from 'Shared/Styles/Props/margin';
import * as sizes from 'Shared/Styles/Props/size';
export const NONE: React.CSSProperties = {
};

const ABSOLUTE: React.CSSProperties = {
    display: 'absolute'
};

export const FLEX_COLUMN: React.CSSProperties = {
    flexDirection: 'column'
};

export const ABSOLUTE_CENTER: React.CSSProperties = {
    ...ABSOLUTE,
    ...margins.MARGIN_MENU_HEIGHT_AUTO,
    left: 0,
    right: 0

};

export const SIZE_SMALL: React.CSSProperties = {
    ...sizes.W300pxH300px
};
export const SIZE_NORMAL: React.CSSProperties = {
    ...sizes.W600pxH400px
};

export type OptionTypes = 'NONE' | 'FLEX_COLUMN' | 'ABSOLUTE_CENTER' | 'SIZE_SMALL' | 'SIZE_NORMAL';
