import * as React from 'react';
import * as bs from 'Shared/Styles/Props/boxShadow';
import * as borders from 'Shared/Styles/Props/border';
import * as paddings from 'Shared/Styles/Props/padding';
import { BACKGROUND_NORMAL } from 'Shared/Styles/Props/background';

export const MAIN: React.CSSProperties = {
};

export const BASE_CONTAINER_: React.CSSProperties = {
    ...bs.BOX_SHADOW_EVEN,
    ...paddings.PADDING_MEDIUM,
    ...borders.BORDER_CONTAINER,
    ...BACKGROUND_NORMAL
};

export const FLEX_CONTAINER: React.CSSProperties = {
    display: 'flex',
    ...BASE_CONTAINER_
};

export const FLEX_WRAPPER: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'space-between'
};

export const DEFAULT: React.CSSProperties = {
};

export type WrapperTypes = 'DEFAULT' | 'FLEX_WRAPPER' | 'MAIN' | 'BASE_CONTAINER_' | 'FLEX_CONTAINER';
