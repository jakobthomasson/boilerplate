import * as React from 'react';
import { styled } from 'styletron-react';

type Props = React.DOMAttributes<{}> & {
    url: string;
};

// const circularImgCssProps: React.CSSProperties = {
//     borderTopLeftRadius: '50%',
//     borderTopRightRadius: '50%',
//     borderBottomLeftRadius: '50%',
//     borderBottomRightRadius: '50%',
// };

const CircularImg = styled('img', () => ({
    // ...circularImgCssProps,
    height: '100%',
    width: '100%',
    minWidth: '100%',
    minHeight: '100%'
}));

class Image extends React.Component<Props> {
    public render() {
        return (
            <CircularImg {...this.props as any} src={this.props.url || ''} />
        );
    }
}

export default Image;
