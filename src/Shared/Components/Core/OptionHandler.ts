import * as wrapperOptions from './Wrapper/options';

type OptionType = 'Wrapper' | 'Ul' | 'Li';

export default class OptionsHandler {
    cssProps: React.CSSProperties;
    selectedOptions: string[];
    optionType: OptionType;
    allOptions: any;

    constructor(options: string[], optionType: OptionType) {
        this.selectedOptions = options;
        this.optionType = optionType;
        this.cssProps = {};

        switch (this.optionType) {
            case 'Wrapper':
                this.allOptions = wrapperOptions;
                break;
            default:
                this.allOptions = wrapperOptions;
                break;
        }
        this.get();
    }

    get() {
        this.selectedOptions.map((option: string) => {
            this.cssProps = { ...this.cssProps, ...this.allOptions[option] };
        });
        return this.cssProps;
    }
}
