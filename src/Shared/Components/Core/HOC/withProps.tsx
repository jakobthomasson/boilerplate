import * as React from 'react';
import * as spacing from 'Shared/Styles/Settings/spacing';

export type StandardProps = {
    margin?: spacing.Spacings;
    marginTop?: spacing.Spacings;
    marginRight?: spacing.Spacings;
    marginBottom?: spacing.Spacings;
    marginLeft?: spacing.Spacings;

    padding?: spacing.Spacings;
    paddingTop?: spacing.Spacings;
    paddingRight?: spacing.Spacings;
    paddingBottom?: spacing.Spacings;
    paddingLeft?: spacing.Spacings;
}

export const Defaults = ({
    padding = 'NONE',
    paddingTop = 'NONE',
    paddingRight = 'NONE',
    paddingBottom = 'NONE',
    paddingLeft = 'NONE',
    margin = 'NONE',
    marginTop = 'NONE',
    marginRight = 'NONE',
    marginBottom = 'NONE',
    marginLeft = 'NONE',
}: StandardProps) => ({
        padding: spacing[padding],
        paddingTop: spacing[paddingTop],
        paddingRight: spacing[paddingRight],
        paddingBottom: spacing[paddingBottom],
        paddingLeft: spacing[paddingLeft],
        margin: spacing[margin],
        marginTop: spacing[marginTop],
        marginRight: spacing[marginRight],
        marginBottom: spacing[marginBottom],
        marginLeft: spacing[marginLeft],
    });

export default function withStandardProps(WrappedComponent: any) {

    class Standard extends React.Component<StandardProps> {
        constructor(props: StandardProps) {
            super(props);

        }

        render() {

            return <WrappedComponent {...this.props} />;
        }
    }

    return Standard;
}
