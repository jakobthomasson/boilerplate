import * as React from 'react';
import { styled } from 'styletron-react';
import { HEADER_HEIGHT, } from 'Shared/Styles/Settings/base';
import { PRIMARY_DARK } from 'Shared/Styles/Settings/colors';

type Props = {
};

const HeaderElm = styled('header', () => ({
    position: 'absolute',
    maxHeight: HEADER_HEIGHT,
    minHeight: HEADER_HEIGHT,
    backgroundColor: PRIMARY_DARK,
    top: '0',
    left: '0',
    zIndex: 10,
    width: '100%'
}));

class Header extends React.Component<Props> {

    render() {
        return (
            <HeaderElm >
                {this.props.children}
            </HeaderElm>
        );
    }
}

export default Header;
