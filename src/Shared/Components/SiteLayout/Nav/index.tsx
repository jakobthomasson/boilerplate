import * as React from 'react';
import { styled } from 'styletron-react';
import { MENU_HEIGHT } from 'Shared/Styles/Settings/base';
import { PRIMARY_LIGHT } from 'Shared/Styles/Settings/colors';

const Navigation = styled('nav', () => ({
    position: 'absolute',
    width: '100%',
    bottom: 0,
    height: MENU_HEIGHT,
    backgroundColor: PRIMARY_LIGHT
}));

const Nav = (props: any) => (
    <Navigation >
        {props.children}
    </Navigation>
);

export default Nav;
