import * as React from 'react';
// import { styled } from 'styletron-react';
import Wrapper from 'Shared/Components/Core/Wrapper';
import * as wrapperTypes from 'Shared/Components/Core/Wrapper/types';

type ContainerTypes = 'MAIN';

type Props = {
    containerType: ContainerTypes;
    children: any;
};

type State = {
    wrapperType: wrapperTypes.WrapperTypes;
};

class Container extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            wrapperType: 'DEFAULT'
        };
    }

    componentDidMount() {
        let wrapperType: wrapperTypes.WrapperTypes;
        switch (this.props.containerType) {
            case 'MAIN':
                wrapperType = 'MAIN';
                break;
            default:
                break;
        }
        this.setState({ wrapperType });
    }

    render() {
        return (
            <Wrapper type={this.state.wrapperType} >
                {this.props.children}
            </Wrapper>
        );
    }
}

export default Container;
