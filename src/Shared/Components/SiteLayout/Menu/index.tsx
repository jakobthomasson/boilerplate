import * as React from 'react';
import { RouteItem } from 'routes';
import { withRouter } from 'react-router-dom';
import { styled } from 'styletron-react';
import MenuItem from './MenuItem';

import { MENU_HEIGHT, HEADER_HEIGHT } from 'Shared/Styles/Settings/base';
import { PRIMARY, PRIMARY_DARK } from 'Shared/Styles/Settings/colors';

type Props = {
    menuItems: RouteItem[];
    history?: any;
};

class Menu extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    onMenuItemClick = (path: string): void => {
        this.props.history.push(path);
        console.log(path);
    }

    render() {
        return (
            <Header>
                <Navigation>
                    <MenuList>
                        {
                            this.props.menuItems.map((menuItem: RouteItem, index: number) => (
                                <MenuItem key={index}
                                    menuItem={menuItem}
                                    isActive={this.props.history.location.pathname.indexOf(menuItem.path) !== -1}
                                    onMenuItemClick={this.onMenuItemClick} history={this.props.history} />
                            ))
                        }
                    </MenuList>
                </Navigation>
            </Header>
        );
    }
}

export default withRouter(Menu);

const Header = styled('header', () => ({
    position: 'relative',
    maxHeight: HEADER_HEIGHT,
    minHeight: HEADER_HEIGHT,
    backgroundColor: PRIMARY_DARK,
    top: '0',
    left: '0',
    zIndex: 10,
    width: '100%',
}));

const Navigation = styled('nav', () => ({
    position: 'absolute',
    width: '100%',
    bottom: 0,
    backgroundColor: PRIMARY,
    height: MENU_HEIGHT,
    display: 'flex'

}));

const MenuList = styled(('ul'), () => ({
    display: 'flex',
    justifyContent: 'space-around',
    margin: 0,
    padding: 0,
    listStyle: 'none',
    flexDirection: 'inherit'
}));
