import * as React from 'react';
import { RouteItem } from 'routes';
import { styled } from 'styletron-react';
import { PRIMARY_TEXT, PRIMARY_LIGHT } from 'Shared/Styles/Settings/colors';
import { LARGE } from 'Shared/Styles/Settings/spacing';
import { FONT_HUGE } from 'Shared/Styles/Settings/fontSize';
import { BORDER_MENU_SELECTED, BORDER_MENU_NOT_SELECTED } from 'Shared/Styles/Props/border';
import { ALIGN_CENTER } from 'Shared/Styles/Props/align';
import SubMenu from './SubMenu';

type Props = {
    menuItem: RouteItem;
    isActive: boolean;
    history: any;
    onMenuItemClick: (path: string) => void;
};

type State = {
    isMouseOver: boolean;
    showSubMenu: boolean;
};

const Li = styled('li', ({ cssProps }: { cssProps?: React.CSSProperties }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    height: '100%',
    minHeight: '100%',
    maxHeight: '100%',
    minWidth: '15em',
    maxWidth: '15em',
    flexDirection: 'column',
    cursor: 'pointer',
    ':hover': {
        background: PRIMARY_LIGHT
    },
    ...BORDER_MENU_NOT_SELECTED,
    ...cssProps,
}));

const TextWrapper = styled('p', () => ({
    display: 'flex',
    fontWeight: 'bold',
    fontSize: FONT_HUGE,
    maxWidth: '100%',
    minWidth: '100%',
    height: '100%',
    minHeight: '100%',
    maxHeight: '100%',
    color: PRIMARY_TEXT,
    padding: `0 ${LARGE}`,
    ...ALIGN_CENTER,
}));

class MenuItem extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isMouseOver: false,
            showSubMenu: false
        };
    }

    onMouseEnter() {
        if (this.props.menuItem.children) {
            this.showSubMenu(true);
        }
        this.setState({ isMouseOver: true });
    }

    onMouseLeave() {
        if (this.props.menuItem.children) {
            this.showSubMenu(false);
        }
        this.setState({ isMouseOver: false });
    }

    showSubMenu = (show: boolean) => {
        this.setState({ showSubMenu: show });
    }

    render() {
        return (

            <Li onMouseEnter={() => this.onMouseEnter()} onMouseLeave={() => this.onMouseLeave()}
                cssProps={(this.props.isActive) && { ...BORDER_MENU_SELECTED }}>
                <TextWrapper onClick={() => this.props.onMenuItemClick(this.props.menuItem.path)}>
                    {this.props.menuItem.menuName}
                </TextWrapper>

                {this.props.menuItem.children &&
                    <SubMenu menuItems={this.props.menuItem.children} isOpen={this.state.showSubMenu} showSubMenu={this.showSubMenu}
                        onMenuItemClick={this.props.onMenuItemClick} history={this.props.history} />
                }
            </Li >
        );
    }
}

export default MenuItem;
