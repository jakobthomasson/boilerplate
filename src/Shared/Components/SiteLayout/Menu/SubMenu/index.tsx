import * as React from 'react';
import { RouteItem } from 'routes';
import { styled } from 'styletron-react';
import SubMenuItem from './SubMenuItem';
import { PRIMARY_LIGHT } from 'Shared/Styles/Settings/colors';

type Props = {
    menuItems: RouteItem[];
    history: any;
    isOpen: boolean;
    showSubMenu: (show: boolean) => void;
    onMenuItemClick: (path: string) => void;
};

class SubMenu extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <SubMenuWrapper isOpen={this.props.isOpen} onMouseLeave={() => this.props.showSubMenu(false)}>

                <SubMenuList>
                    {
                        this.props.menuItems.map((menuItem: RouteItem, index: number) => (
                            <SubMenuItem key={index}
                                menuItem={menuItem}
                                isActive={this.props.history.location.pathname === menuItem.path}
                                onMenuItemClick={this.props.onMenuItemClick} />
                        ))
                    }
                </SubMenuList>
            </SubMenuWrapper>
        );
    }
}

export default SubMenu;

const SubMenuList = styled(('ul'), () => ({
    display: 'flex',
    justifyContent: 'flex-start',
    margin: 0,
    marginTop: '1rem',
    padding: 0,
    listStyle: 'none',
    flexDirection: 'column',
    transition: 'all 10s'
}));

const SubMenuWrapper = styled(('div'), ({ isOpen }: { isOpen: boolean }) => ({
    position: 'absolute',
    top: '100%',
    width: '15em',
    maxHeight: isOpen ? 'auto' : 0,
    overflow: 'hidden',
    background: PRIMARY_LIGHT,
    justifyContent: 'space-around',
    margin: 0,
    padding: 0,
    listStyle: 'none',
    flexDirection: 'column',
}));
