import * as React from 'react';
// import { Link } from 'react-router-dom';
import { RouteItem } from 'routes';
import { styled } from 'styletron-react';

import { PRIMARY_TEXT, SECONDARY } from 'Shared/Styles/Settings/colors';

import { FONT_MEDIUM } from 'Shared/Styles/Settings/fontSize';
import { BORDER_SUBMENU_DOWN } from 'Shared/Styles/Props/border';
import { MARGIN_MEDIUM } from 'Shared/Styles/Props/margin';

// import { BOX_SHADOW_TOGGLE_INPUT } from 'Shared/Styles/Props/boxShadow';
type Props = {
    menuItem: RouteItem;
    isActive: boolean;
    onMenuItemClick: (path: string) => void;
};

type State = {
    isMouseOver: boolean;
    showSubMenu: boolean;
};

const Li = styled(('li'), () => ({
    display: 'flex',
    justifyContent: 'space-between',
    height: '100%',
    minHeight: '100%',
    maxHeight: '100%',
    alignItems: 'center',
    cursor: 'pointer',
    ...BORDER_SUBMENU_DOWN,
    ...MARGIN_MEDIUM,
}));

const TextWrapper = styled('p', () => ({
    fontWeight: 'bold',
    fontSize: FONT_MEDIUM,
    color: PRIMARY_TEXT,
    textTransform: 'uppercase',
    marginBottom: 0
}));

const Rectangle = styled('div', ({ isActive }: { isActive: boolean }) => ({
    minWidth: '0.7rem',
    maxWidth: '0.7rem',
    minHeight: '0.7rem',
    maxHeight: '0.7rem',
    borderRadius: '50%',
    background: isActive ? SECONDARY : 'transparent'

}));

class MenuItem extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isMouseOver: false,
            showSubMenu: false
        };
    }

    onMouseEnter() {
        this.setState({ isMouseOver: true });
    }

    onMouseLeave() {
        this.setState({ isMouseOver: false });
    }

    render() {
        return (
            <Li onMouseEnter={() => this.setState({ isMouseOver: true })} onMouseLeave={() => this.setState({ isMouseOver: false })}
                onClick={() => this.props.onMenuItemClick(this.props.menuItem.path)}  >
                <TextWrapper>
                    {this.props.menuItem.menuName}
                </TextWrapper>
                <Rectangle isActive={this.props.isActive || this.state.isMouseOver} />
            </Li >
        );
    }
}

export default MenuItem;
