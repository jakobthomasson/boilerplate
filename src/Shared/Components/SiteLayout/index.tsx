import * as React from 'react';
import { RouteItem, getRoutes } from 'routes';
import { Switch, Route } from 'react-router';
import { styled } from 'styletron-react';
import Menu from './Menu';

type Props = {
};

type State = {
    routeItems: RouteItem[]
};

const Site = styled('div', () => ({
    height: '100%',
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'column'
}));

class SiteLayout extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            routeItems: getRoutes()
        };
    }
    render() {
        return (
            <Site>
                <Menu menuItems={this.state.routeItems} />
                <Switch>
                    {
                        this.state.routeItems.map((routeItem: RouteItem, index: number) => {
                            return (
                                <Route key={index} path={routeItem.path} component={routeItem.component} />
                            );
                        })
                    }
                </Switch>
            </Site>
        );
    }
}

export default SiteLayout;
