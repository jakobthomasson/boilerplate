import * as React from 'react';
import * as spacing from 'Shared/Styles/Settings/spacing';
import * as colors from 'Shared/Styles/Settings/colors';

export const BORDER_DOWN: React.CSSProperties = {
    borderBottom: `${spacing.THIN} solid ${colors.PRIMARY_LIGHT}`
};

export const BORDER_SUBMENU_DOWN: React.CSSProperties = {
    borderBottom: `${spacing.THIN} solid ${colors.PRIMARY_TEXT}`
};

export const BORDER_SUBMENU_DOWN_SELECTED: React.CSSProperties = {
    borderBottom: `${spacing.THIN} solid ${colors.SECONDARY_LIGHT}`
};

export const BORDER_CONTAINER: React.CSSProperties = {
    border: `${spacing.THINK} solid ${colors.SECONDARY}`,
    borderRadius: spacing.MEDIUM
};
export const BORDER_DROPDOWN: React.CSSProperties = {
    border: `${spacing.THINK} solid ${colors.SECONDARY_DARK}`,
    borderRadius: spacing.THIN
};

export const BORDER_TABLE: React.CSSProperties = {
    border: `${spacing.THIN} solid ${colors.PRIMARY}`
};

export const DEFAULT: React.CSSProperties = {
};

export const BORDER_MENU_SELECTED: React.CSSProperties = {
    borderBottom: `${spacing.SMALL} solid ${colors.SECONDARY_DARK}`
};
export const BORDER_MENU_NOT_SELECTED: React.CSSProperties = {
    borderBottom: `${spacing.SMALL} solid ${colors.SECONDARY}`
};

export type BorderTypes = 'BORDER_DOWN' | 'DEFAULT' | 'STANDARD' | 'BORDER_MENU_SELECTED' | 'BORDER_MENU_NOT_SELECTED' | 'BORDER_SUBMENU_DOWN' | 'BORDER_SUBMENU_DOWN_SELCETED';
