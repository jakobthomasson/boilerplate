import * as React from 'react';
import * as colors from 'Shared/Styles/Settings/colors';

export const GRADIENT_MAIN_VERTICAL: React.CSSProperties = {
    background: `linear-gradient(${colors.PRIMARY_LIGHT},${colors.PRIMARY} 50%)`
};

export type BoxShadowTypes = 'GRADIENT_MAIN_VERTICAL';
