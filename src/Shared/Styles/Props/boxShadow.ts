import * as React from 'react';
import * as spacing from 'Shared/Styles/Settings/spacing';
import * as colors from 'Shared/Styles/Settings/colors';

export const BOX_SHADOW_DOWN_RIGHT: React.CSSProperties = {
    boxShadow: `0px 4px 3px -3px ${colors.PRIMARY_LIGHT}`
};

export const BOX_SHADOW_BOTTOM_ONLY = {
    boxShadow: `${spacing.THIN} ${spacing.THIN} ${spacing.MEDIUM} ${colors.PRIMARY_LIGHT}`
};

export const BOX_SHADOW_EVEN = {
    boxShadow: `0 0 ${spacing.MEDIUM} ${spacing.THINK} ${colors.PRIMARY_DARK}`,
};

export const BOX_SHADOW_TEXT_INPUT = {
    boxShadow: `inset 0px 0px ${spacing.MEDIUM} ${spacing.THIN} ${colors.PRIMARY_LIGHT}`,
};

export const BOX_SHADOW_TOGGLE_INPUT = {
    boxShadow: `inset 0px 0px ${spacing.SMALL} ${spacing.THIN} ${colors.PRIMARY_DARK}`,
};
export const DEFAULT = {
};

export type BoxShadowTypes = 'BOX_SHADOW_DOWN_RIGHT' | 'BOX_SHADOW_BOTTOM_ONLY' | 'BOX_SHADOW_EVEN' | 'DEFAULT' | 'BOX_SHADOW_TOGGLE_INPUT';
