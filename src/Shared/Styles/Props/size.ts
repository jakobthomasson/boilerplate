import * as React from 'react';

/*
Sizes naming convention.
*/
export const W300pxH300px: React.CSSProperties = {
    width: '300px',
    height: '300px'
};

export const W600pxH400px: React.CSSProperties = {
    width: '600px',
    height: '400px'
};

export const DEFAULT: React.CSSProperties = {
};

export type BorderTypes = 'BACKGROUND_DARK' | 'DEFAULT';
