import * as React from 'react';
import * as spacing from 'Shared/Styles/Settings/spacing';

export const DEFAULT: React.CSSProperties = {
};

export const PADDING_NONE_AUTO: React.CSSProperties = {
    padding: `${spacing.SMALL} ${spacing.AUTO}`
};
export const PADDING_MEDIUM: React.CSSProperties = {
    padding: `${spacing.MEDIUM}`
};
export const PADDING_LARGE: React.CSSProperties = {
    padding: `${spacing.LARGE}`
};
export const PADDING_HUGE: React.CSSProperties = {
    padding: `${spacing.HUGE}`
};

export type PaddingTypes = 'PADDING_NONE_AUTO' | 'DEFAULT' | 'PADDING_MEDIUM' | 'PADDING_LARGE' | 'PADDING_HUGE';
