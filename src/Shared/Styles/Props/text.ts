import * as React from 'react';
// import * as spacing from 'Shared/Styles/Settings/spacing';
import * as colors from 'Shared/Styles/Settings/colors';
import * as fontSizes from 'Shared/Styles/Settings/fontSize';

export const TEXT_MENU_FIRST_LEVEL: React.CSSProperties = {
    fontSize: fontSizes.FONT_HUGE,
    color: colors.PRIMARY_TEXT
};

export const DEFAULT: React.CSSProperties = {
};

export type BorderTypes = 'TEXT_MENU_FIRST_LEVEL' | 'DEFAULT';
