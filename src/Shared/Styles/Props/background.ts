import * as React from 'react';
import * as colors from 'Shared/Styles/Settings/colors';

export const BACKGROUND_DARK: React.CSSProperties = {
    backgroundColor: colors.PRIMARY_DARK
};

export const BACKGROUND_NORMAL: React.CSSProperties = {
    backgroundColor: colors.BACKGROUND
};
export const DEFAULT: React.CSSProperties = {
    backgroundColor: colors.PRIMARY
};

export type BorderTypes = 'BACKGROUND_DARK' | 'DEFAULT' | 'BACKGROUND_NORMAL';
