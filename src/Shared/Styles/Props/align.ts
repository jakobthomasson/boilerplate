import * as React from 'react';

export const ALIGN_CENTER: React.CSSProperties = {
    justifyContent: 'space-around',
    alignItems: 'center'
};

export const DEFAULT: React.CSSProperties = {
};

export type BorderTypes = 'ALIGN_CENTER' | 'DEFAULT';
