import * as React from 'react';
import * as spacing from 'Shared/Styles/Settings/spacing';
import { MENU_HEIGHT } from 'Shared/Styles/Settings/base';
// NONE
export const DEFAULT: React.CSSProperties = {
};

export const MARGIN_NONE_AUTO: React.CSSProperties = {
    margin: `${spacing.SMALL} ${spacing.AUTO}`
};

export const MARGIN_MENU_HEIGHT_AUTO: React.CSSProperties = {
    margin: `${MENU_HEIGHT} ${spacing.AUTO} 0 ${spacing.AUTO} `
};

export const MARGIN_LARGE_NONE: React.CSSProperties = {
    margin: `${spacing.LARGE} ${spacing.NONE}`
};

export const MARGIN_HUGE_NONE: React.CSSProperties = {
    margin: `${spacing.HUGE} ${spacing.NONE}`
};

export const MARGIN_NONE_LARGE: React.CSSProperties = {
    margin: `${spacing.NONE} ${spacing.LARGE}`
};

export const MARGIN_NONE_LARGE_NONE_NONE: React.CSSProperties = {
    marginRight: spacing.LARGE
};

export const MARGIN_MEDIUM: React.CSSProperties = {
    margin: spacing.MEDIUM
};

export type MarginTypes = 'MARGIN_NONE_AUTO' | 'DEFAULT' | 'MARGIN_LARGE_NONE' |
    'MARGIN_HUGE_NONE' | 'MARGIN_NONE_LARGE' | 'MARGIN_MENU_HEIGHT_AUTO' |
    'MARGIN_NONE_LARGE_NONE_NONE' | 'MARGIN_MEDIUM';
