
export const FONT_SMALL: string = '10px';
export const FONT_MEDIUM: string = '14px';
export const FONT_LARGE: string = '18px';
export const FONT_HUGE: string = '24px';
export const FONT_GIGANTIC: string = '32px';
export const FONT_MONSTROUS: string = '48px';

export type fontSizes = 'FONT_SMALL' | 'FONT_MEDIUM' | 'FONT_LARGE' | 'FONT_HUGE' | 'FONT_GIGANTIC' | 'FONT_MONSTROUS';
