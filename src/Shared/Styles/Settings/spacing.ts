export const NONE = '0';
export const THIN = '1px';
export const THINK = '2px';
export const TINY = '0.125rem';
export const SMALL = '0.25rem';
export const MEDIUM = '0.5rem';
export const LARGE = '1rem';
export const HUGE = '2rem';
export const GIGANTIC = '3rem';
export const MONSTROUS = '4rem';
export const AUTO = 'auto';

export type Spacings = 'NONE' | 'THIN' | 'THINK' | 'TINY' | 'SMALL' | 'MEDIUM' | 'LARGE' | 'HUGE' | 'GIGANTIC' | 'AUTO' | 'MONSTROUS';
