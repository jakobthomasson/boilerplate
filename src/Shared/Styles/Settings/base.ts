
export const SITE_MAX_WIDTH: string = '1280px';
export const HEADER_HEIGHT: string = '10rem';
export const MENU_HEIGHT: string = '3rem';
