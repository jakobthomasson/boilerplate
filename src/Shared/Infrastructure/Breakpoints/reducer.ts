import { SwitchBreakpointAction, SWITCH_BREAKPOINT } from './action-creators';
import { Action } from 'Shared/Store';
import { current } from './';

const initialState: number = current();

export default function (state: number = initialState, action: Action): number {
    switch (action.type) {
        case SWITCH_BREAKPOINT:
            return (action as SwitchBreakpointAction).newBreakpoint;
        default:
            return state;
    }
}
