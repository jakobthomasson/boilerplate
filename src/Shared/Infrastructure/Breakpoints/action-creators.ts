import { Action } from 'Shared/Store';

export const SWITCH_BREAKPOINT = 'SWITCH_BREAKPOINT';

export type SwitchBreakpointAction = Action & {
  newBreakpoint: number;
};

const breakpointCookieName = 'breakpoint';
export function switchBreakpoint(newBreakpoint: number) {
  document.cookie = breakpointCookieName + '=' + newBreakpoint + ';path=/;max-age=' + 60 * 60 * 24 * 365;

  return {
    type: SWITCH_BREAKPOINT,
    newBreakpoint
  } as SwitchBreakpointAction;
}
