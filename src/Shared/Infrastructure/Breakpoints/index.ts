import { switchBreakpoint } from './action-creators';
import { on } from '../eventHandling';

export const INIT = 0;
export const MICRO = 320;
export const TINY = 480;
export const SMALL = 768;
export const MEDIUM = 992;
export const LARGE = 1200;
export const HUGE = 1560;

const breakpoints = {
    init: INIT,
    micro: MICRO,
    tiny: TINY,
    small: SMALL,
    medium: MEDIUM,
    large: LARGE,
    huge: HUGE
};

export function breakpointFromWidth(width: number) {
    const keys = Object.keys(breakpoints);
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        const curr = breakpoints[key];
        const next = breakpoints[keys[i + 1]] || Infinity;
        if (curr <= width && next > width) {
            return breakpoints[key];
        }
    }

    console.error(
        'Could not match a breakpoint. window.innerWidth:', window.innerWidth,
        'keys:', keys,
        'breakpoints:', breakpoints
    );
}

export function current() {
    let currentWidth = window.innerWidth;
    return breakpointFromWidth(currentWidth);
}

export function setupResizeListener(store) {
    let currentBreakpoint = current();

    // We use `Root/on` here to enable the throttled variant of `window.addEventListener('resize', ...`
    on('resize', () => {
        const newBreakpoint = current();
        if (currentBreakpoint !== newBreakpoint) {
            currentBreakpoint = newBreakpoint;
            store.dispatch(switchBreakpoint(currentBreakpoint));
        }
    });
}
