
type EventHandler = (e: Event) => void;
let listeners: { [index: string]: Array<EventHandler>; } = {};

export function on(event: string, callback: EventHandler) {
  const events = event.split(' ');
  events.forEach(eventName => {
    if (!listeners[eventName]) {
      listeners[eventName] = [];
    }

    listeners[eventName].push(callback);
    handleWindowSubscription(eventName);
  });

  // We return a function here that you can call to unscribe to the event.
  // This instead of exporting an `off` function where you have to pass
  // the same function you passed to `on` which makes it impossible to
  // pass an inline function to `on`.
  return () => events.forEach(
    eventName => {
      listeners[eventName] = listeners[eventName].filter(
        listener => listener !== callback
      );
      handleWindowSubscription(eventName);
    }
  );
}

const windowEvents = {
  resize: throttleEvent,
  scroll: throttleEvent
};

// Pass `window` events defined in `windowEvents` to this module. Should be
// invoked when `listeners` has been manipulated
function handleWindowSubscription(eventName: string) {
  if (windowEvents[eventName]) {
    if (hasSubscribers(eventName)) {
      window.addEventListener(eventName, windowEvents[eventName]);
    } else {
      window.removeEventListener(eventName, windowEvents[eventName]);
    }
  }
}

export function triggerEvent<T extends Event>(e: T) {
  if (!hasSubscribers(e.type)) {
    return;
  }
  listeners[e.type].forEach(listener => {
    listener(e);
  });
}

function hasSubscribers(eventName: string) {
  return listeners[eventName] && listeners[eventName].length > 0;
}

const throttling: Array<string> = [];

// Some events than can fire at a high rate and execute computationally
// expensive operations such as DOM modifications. Those events should
// be throttled or debounced, such as `scroll` and `resize`.
function throttleEvent<T extends Event>(e: T) {
  if (!hasSubscribers(e.type) || throttling.indexOf(e.type) !== -1) {
    return;
  }
  throttling.push(e.type);
  window.requestAnimationFrame(() => {
    triggerEvent(e);
    throttling.splice(throttling.indexOf(e.type), 1);
  });
}
