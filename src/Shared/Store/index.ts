export { State } from './state';
export { default as reducers } from './reducers';
export { default as createStore } from './create-store';
export { default as connect } from './connect';

type ActionOrFunction = Action | Function;

export type ActionSignature = () => Promise<any>;

export type Action = { type: string };
export type Dispatch = (action: ActionOrFunction) => any;
