
export default function deepFreeze(o: Object) {
  if (Array.isArray(o)) {
    o.forEach(i => deepFreeze(o[i]));
  } else if (typeof o === 'object' && o !== undefined) {
    Object.keys(o).forEach(key => deepFreeze(o[key]));
  }
  Object.freeze(o);

  return o;
}
