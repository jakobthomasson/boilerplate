import { connect } from 'react-redux';
import { Store } from 'redux';
import { State, Dispatch } from './index';
import { MergeProps, Options, AdvancedComponentDecorator } from 'react-redux';

interface MapStateToProps<TStateProps, TOwnProps> {
    (state: State, ownProps?: TOwnProps): TStateProps;
}

export type Store = Store<State>;

export default function <TOwnProps>(
    mapStateToProps: MapStateToProps<State, TOwnProps>,
    mapDispatchToProps?: (dispatch: Dispatch) => any,
    mergeProps?: MergeProps<State, any, TOwnProps, any>,
    options?: Options
): AdvancedComponentDecorator<{}, TOwnProps> {
    return connect.apply(this, [mapStateToProps, mapDispatchToProps, mergeProps, options]);
}
