import { State, Action } from './index';
// import CurrentPage from 'Shared/Components/CurrentPage/reducer';
import Breakpoints from 'Shared/Infrastructure/Breakpoints/reducer';
import { breakpointFromWidth } from 'Shared/Infrastructure/Breakpoints';
import deepFreeze from './deep-freeze';
// import Account from 'Client/Account/reducer';
// import Bookings from 'Client/BookingsBox/reducer';
// import User from 'Client/Account/Users/reducer';
// tslint:disable-next-line
export default (state: State, action: Action): State => {   // tslint:disable-line
    console.log(state);
    return deepFreeze({
        //  currentPage: CurrentPage(state.currentPage, action),
        //  currentUser: User(state.currentUser, action),
        //  accountBox: Account(state.accountBox, action),
        currentBreakpoint: Breakpoints(breakpointFromWidth(window.innerWidth), action)
    });
};

