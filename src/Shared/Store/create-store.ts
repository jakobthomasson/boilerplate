import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { default as thunk } from 'redux-thunk';
import * as Store from './index';

export default function (initialState: Store.State) {
    // Build middleware. These are functions that can process the actions before they reach the store.
    // const windowIfDefined = typeof window === 'undefined' ? null : window as any;
    // const devToolsExtension = windowIfDefined && windowIfDefined.devToolsExtension as () => GenericStoreEnhancer;// If devTools is installed, connect to it
    const createStoreWithMiddleware = composeWithDevTools(applyMiddleware(thunk)
        // This enables the Redux Dev Tools for Chrome:
        // https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd/related
        // devToolsExtension ? devToolsExtension() : f => f
    )(createStore);
    // Combine all reducers and instantiate the app-wide store instance
    const store = createStoreWithMiddleware(Store.reducers, initialState);
    // const store = createStore(Store.reducers, initialState, compose(applyMiddleware(thunk), devToolsExtension ? devToolsExtension() : f => f));
    return store;
}

