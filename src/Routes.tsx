// import * as React from 'react';
import { RouteProps } from 'react-router';
let basePath: string = '';
// import Home from 'Components/Home';
// import Example from 'Components/React101/Example';
import Finished from 'Components/React101/Replicate';

export type RouteItem = RouteProps & {
  menuName: string;
  children?: RouteItem[];
};

export function getMenuItems(): RouteItem[] {
  let routes = getRoutes().filter(route => route.component);
  routes.forEach((value: RouteItem, index: number) => { routes[index].path = basePath + '/' + value.path; });
  return routes;
}

export function getRoutes(): RouteItem[] {
  // Checks if the user is logged in, and if the user have the required role.
  const routes: RouteItem[] = [
    {
      menuName: 'Färdigt',
      component: Finished,
      path: '/'
    },
    {
      menuName: 'Vår kod',
      component: Finished,
      path: '/ourcode'
    },
    // {
    //   menuName: 'React101',
    //   path: '/react101',
    //   children: [
    //     {
    //       menuName: 'Färdigt exempel',
    //       component: Finished,
    //       path: '/react101/finished'
    //     },
    //     {
    //       menuName: 'Vår kod',
    //       component: Example,
    //       path: '/react101/example'
    //     }
    //   ]
    // }

  ];

  return routes;
}
