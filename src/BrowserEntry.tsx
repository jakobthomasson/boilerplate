import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './App';

// Global CSS
require('./Shared/Styles/Global/base.css');
require('./Shared/Styles/Global/spinner.css');

const render = (Component: any) => {
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer >
        ,
        document.getElementById('root')
    );
};

render(App);

if (module.hot) {
    module.hot.accept(() => {
        render(App);
    });
}
