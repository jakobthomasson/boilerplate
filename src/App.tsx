import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { StyletronProvider } from 'styletron-react';
import Styletron from 'styletron-client';

import { Provider } from 'react-redux';
import { createStore, State } from 'Shared/Store';
import { setupResizeListener } from 'Shared/Infrastructure/Breakpoints';
import SiteLayout from 'Shared/Components/SiteLayout';

// import { getRoutes, RouteItem } from './routes';

const styleSheet = document.createElement('style');
document.head.appendChild(styleSheet);
const styletron = new Styletron([styleSheet]);

// Get the application-wide store instance, prepopulating with state from the server where available.
const initialState: State = {};
const store = createStore(initialState);
setupResizeListener(store);

const App = () => (
    <Provider store={store}>
        <StyletronProvider styletron={styletron}>
            <BrowserRouter>
                <SiteLayout />
            </BrowserRouter>
        </StyletronProvider>
    </Provider>
);

export default App;
