const path = require('path');

const includePath = path.join(__dirname, '..', 'src');
console.log(includePath);
module.exports = {
  includePath: includePath,
  entry: path.join(includePath , 'BrowserEntry.tsx'),
  output: path.join(__dirname, '..', 'dist'),
  nodeModules: path.resolve(__dirname, '..', 'node_modules'),
  htmlIndex: path.resolve(includePath, 'index.ejs')
}
