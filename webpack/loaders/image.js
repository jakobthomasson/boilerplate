    const defaults = {};

    module.exports = function (options) {
        return {
            loader: 'image-webpack-loader',
            options: Object.assign(defaults, options)
        }
    };
