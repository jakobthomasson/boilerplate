const commonConfig = require('./common');
const WebpackNotifierPlugin = require('webpack-notifier');
const merge = require('webpack-merge');
const proxy = 'localhost:54633';
const paths = require('./paths');
const webpack = require('webpack');

module.exports = function () {
    commonConfig.entry.main.unshift('react-hot-loader/patch');
    return merge(commonConfig, {
        devtool: 'inline-source-map',
        devServer: {
            hot: true,
            contentBase: paths.output,
            publicPath: '/dist/',
            noInfo: true,
            historyApiFallback: true
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new WebpackNotifierPlugin({ alwaysNotify: true }),
            new webpack.NamedModulesPlugin(),
            // prints more readable module names in the browser console on HMR updates
        ]
    })
}();




