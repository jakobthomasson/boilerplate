declare module Api {

    interface BaseStat {
        sty: number,
        fys: number,
        sto: number,
        smi: number,
        int: number,
        per: number,
        vil: number
    }

    interface Gift {
        name: string,
        description: string,
        effects?: Api.Character
    }

    interface Ability {
        name: string,
        lvl: number,
        isAdvanced?: boolean
    }

    interface Character {
        name?: string,
        age?: number,
        class?: 'IMM' | 'MM' | 'MD' | 'PSI' | 'RBT',
        base?: Api.BaseStat,
        gifts?: Gift[],
        abilities?: Ability[],
    }
}